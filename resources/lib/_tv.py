import xbmcplugin
import xbmcgui
import xbmc

import common

import sys
import re

from BeautifulSoup import BeautifulStoneSoup

class Main:

    def __init__( self ):
        if common.args.mode == 'TV' and common.settings['flat_tv_cats']:
            xbmcplugin.setContent(int(sys.argv[1]), 'tvshows')
            self.addShowsList()
            xbmcplugin.endOfDirectory( handle=int( sys.argv[ 1 ] ))
        elif common.args.mode == 'TV_List':
            xbmcplugin.setContent(int(sys.argv[1]), 'tvshows')
            self.addShowsList()
            xbmcplugin.endOfDirectory( handle=int( sys.argv[ 1 ] ))
        elif common.args.mode == 'TV_Seasons':
            xbmcplugin.setContent(int(sys.argv[1]), 'seasons')
            self.addSeasonList()
            xbmcplugin.endOfDirectory( handle=int( sys.argv[ 1 ] ))
        elif common.args.mode == 'TV_Episodes':
            xbmcplugin.setContent(int(sys.argv[1]), 'episodes')
            self.addEpisodeList()
            xbmcplugin.endOfDirectory( handle=int( sys.argv[ 1 ] ))
        elif common.args.mode == 'TV_Clips':
            xbmcplugin.setContent(int(sys.argv[1]), 'episodes')
            self.addClipsList()
        elif common.args.mode == 'TV_Categorical':
            xbmcplugin.setContent(int(sys.argv[1]), 'episodes')
            self.addCategoricalList()
        else:
            #ADD 'ALL SHOWS' to Main Category List
            if common.args.mode == "TV":
                xbmcplugin.setContent(int(sys.argv[1]), 'tvcats')
                #space in string ' All Shows' ensures it is at top of sorted list
                common.addDirectory(' '+xbmc.getLocalizedString(30070), common.args.url, 'TV_List')
                self.addCategories()
                xbmcplugin.endOfDirectory( handle=int( sys.argv[ 1 ] ))
            elif common.args.mode.startswith("TV_cat"):
                self.addShowsList() #if we're in a category view, add videos that match the selected category
                xbmcplugin.setContent(int(sys.argv[1]), 'tvshows')
                if common.args.mode=="TV_cat_sub2":
                    xbmcplugin.endOfDirectory( handle=int( sys.argv[ 1 ]), updateListing=True ) #if we're going into subcategories, don't add to heirchy
                else:
                    xbmcplugin.endOfDirectory( handle=int( sys.argv[ 1 ] ))



    def addCategories( self ):
        html=common.getHTML(common.args.url)
        p=re.compile('href="http://www.hulu.com/browse/alphabetical/.+?(\?channel=.+?)">[^<]')
        match=p.findall(html)
        del html
        for item in match:
            name=item.split("=")[-1]
            if name!='all':
                if common.args.mode == "TV_cat_sub":#SubCategories _ don't add to dir heirachy
                    item=item.split('&')[-1]
                    name = "(Subcategory) "+name.replace("+"," ").title()
                    common.addDirectory(name, common.args.url+"&"+item, "TV_cat_sub2")
                elif "&" in item.split("=")[-2]:#SubCategories
                    item=item.split('&')[-1]
                    name = "(Subcategory) "+name.replace("+"," ").title()
                    common.addDirectory(name, common.args.url+"&"+item, "TV_cat_sub")
                elif common.args.mode!='TV_categories':#RegularCategory
                    name= name.replace("+"," ").title()
                    common.addDirectory(name, common.args.url+item, "TV_cat")
        

    def addShowsList( self ):
        xbmcplugin.addSortMethod(int(sys.argv[1]), xbmcplugin.SORT_METHOD_LABEL)
        xbmcplugin.addSortMethod(int(sys.argv[1]), xbmcplugin.SORT_METHOD_GENRE)

        html=common.getHTML(common.args.url)
        tree=BeautifulStoneSoup(html, convertEntities=BeautifulStoneSoup.HTML_ENTITIES)
        shows=tree.findAll('a', attrs={"class":"show-thumb info_hover"})
        del html
        del tree
        pp = re.compile('description": "(.+?)"[,}]')
        cp = re.compile('channel": "(.+?)"[,}]')
        # with clips
        for show in shows:
            name  = show.contents[0]
            url   = show['href']
            tmp   = show['href'].split('/')[3]
            tmp   = tmp.split('?')[0]
            art   = "http://assets.hulu.com/shows/key_art_"+tmp.replace('-','_')+".jpg"
            #thumb = "http://assets.hulu.com/shows/show_thumbnail_"+tmp.replace('-','_')+".jpg"
            #icon  = "http://assets.hulu.com/shows/show_thumbnail_"+tmp.replace('-','_')+".jpg"
            #Use higher res fanart (key_art) instead of lower res thumbs & icons
            thumb = art
            icon = art
            if common.settings['get_show_plot'] == True:
                json = common.getHTML("http://www.hulu.com/shows/info/"+tmp)
                try:
                    #this needs better regex, or maybe some sort of json parser
                    match = pp.findall(json)
                    plot = match[0].replace('\\','')
                except:
                    plot=xbmc.getLocalizedString(30090)
                try:
                    match = cp.findall(json)
                    genre = match[0]
                except:
                    genre=xbmc.getLocalizedString(30090)
                #hopefully deleting this will help with xbox memory problems
                del json
            else:
                plot=genre=xbmc.getLocalizedString(30090)
            try:
                parent = show.parent.findAll('a', attrs={"class":"full-icon"})
                if parent == None:
                    name += ' '+xbmc.getLocalizedString(30091)
                    genre += ' '+xbmc.getLocalizedString(30091)
                elif common.args.url != common.BASE_TV_URL:
                    common.addDirectory(name, url, "TV_Seasons", art, icon, art, plot, genre)
            except:
                name += ' '+xbmc.getLocalizedString(30091)
                genre += ' '+xbmc.getLocalizedString(30091)
                if common.settings['only_full_episodes'] == False:
                    common.addDirectory(name, url, "TV_Seasons", art, icon, art, plot, genre)

        #if we're doing both clips & full episodes, we need to run through the function again.
        if common.args.url == common.BASE_TV_URL :
            common.args.url = common.BASE_FULLTV_URL
            self.addShowsList()
        
        xbmcplugin.endOfDirectory( handle=int( sys.argv[ 1 ] ))



    def addSeasonList( self ):
        tree=BeautifulStoneSoup(common.getHTML(common.args.url), convertEntities=BeautifulStoneSoup.HTML_ENTITIES)  
        seasons=tree.findAll('td', attrs={"class":re.compile('^vex')})
        p=re.compile('(?:")(http://.+?)(?:")')
        #flatten seasons by settings
        if common.settings['flat_season'] == 1 or (len(seasons) == 1 and common.settings['flat_season'] == 0):
            common.args.mode='TV_Episodes'
            seasonNums=[]
            for season in seasons:
                common.args.name = season.contents[0]
                seasonNums.append(season.contents[0])
                common.args.url=p.findall(season['onclick'])[0]
                self.addEpisodeList( )
            #add clips folder
            rss=tree.findAll('a', attrs={'class':'rss-link'})
            clipRSS = None
            for feed in rss:
                if feed['href'].split('/')[-1]=='clips':
                    clipRSS = feed['href']
            if clipRSS != None:
                common.addDirectory(xbmc.getLocalizedString(30095), clipRSS, "TV_Clips")
            xbmcplugin.endOfDirectory( handle=int( sys.argv[ 1 ] ))

        else:
            #add one folder for each season
            for season in seasons:
                name=season.contents[0]
                url=p.findall(season['onclick'])[0]
                ok=common.addDirectory(name, url, "TV_Episodes")
            #add clips folder
            rss=tree.findAll('a', attrs={'class':'rss-link'})
            for feed in rss:
                if feed['href'].split('/')[-1]=='clips':
                    clipRSS = feed['href']
                    common.addDirectory(xbmc.getLocalizedString(30095), clipRSS, "TV_Clips")
            # Category based videos (including, but not necessarily limited to Volumes, Minisodes, Short Films)
            episodeLinks=tree.findAll(lambda tag: tag.name == 'a' and tag.string == 'Season:Episode')
            p=re.compile("(?:')(/videos/expander.+?)(?:')")
            cp=re.compile('[\?&]category=([^&]*)')
            for episode in episodeLinks:
                url=p.findall(episode['onclick'])[0]
                url="http://hulu.com"+url
                match=cp.search(url)
                if match:
                    name=match.group(1).replace('+',' ')
                    common.addDirectory(name, url, "TV_Categorical")
            xbmcplugin.endOfDirectory( handle=int( sys.argv[ 1 ] ))



    def addEpisodeList( self ):
        #initialize variables
        p=re.compile('(\d+)')#gets last number from "season ##"
        currentSeason=p.findall(common.args.name)[0]
        season=str(currentSeason)
        tag='"srh-bottom-'+season+'", "'
        if len(season)<2:season='0'+season

        p=re.compile('http://www.hulu.com/watch/(\d+)/.*')
        dp = re.compile('description": "(.+?)"[,}]')
        tp = re.compile('thumbnail_url": "(.+?)"[,}]')
        #parse html tree
        js=common.getHTML(common.args.url)
#        print "js", js
        try:
            js=js.split(tag)[1]
        except IndexError: #loaded moreEpisode url which does not contain this string
            pass
        js=js.split('");\n')[0]
        js=js.replace('\\u003c','<')
        js=js.replace('\\u003e','>')
        js=js.replace('\\"', '"')
        tree=BeautifulStoneSoup(js, convertEntities=BeautifulStoneSoup.HTML_ENTITIES)

        eps=tree.findAll('tr')
        try: #occurs for no apparent reason on Xbox
            for item in eps:
                cols=item.findAll('td')
                if cols[0].contents:
                    episode=cols[0].contents[0]
                else:
                    episode="0"
                try:
                    episodeNum=int(episode)
                except AttributeError:
                    #exhausted episodes in the current list, must load another url to get the rest of the episodes
                    url=re.compile('(?:")(http://.+?)(?:")').findall(cols[0]['onclick'])
                    common.args.url=url[0]
                    self.addEpisodeList( )
                    return
                if len(episode)<2:episode='0'+episode
                url=cols[1].contents[0]
                name=url.contents[0]
                url=url['href']
                duration=cols[3].contents[0]
                airdate=cols[4].contents[0]
    #            print episode, name, url, duration, airdate
                match=p.findall(url)
                vid=match[0]
                info='http://www.hulu.com/videos/info/'+vid
                json=common.getHTML(info)
                match = dp.findall(json)
                plot=match[0]
                match = tp.findall(json)
                thumb=match[0]
    #            print plot, thumb
                name = 's'+season+'e'+episode+' '+name
                common.addEpisode(name,url,'TV_play', thumb, thumb, common.args.fanart, plot, 'genre', currentSeason, episodeNum, duration, airdate)
        except TypeError:
            pass

    def addCategoricalList( self ):
        #initialize variables
        p=re.compile('http://www.hulu.com/watch/(\d+)/.*')
        dp = re.compile('description": "(.+?)"[,}]')
        tp = re.compile('thumbnail_url": "(.+?)"[,}]')
        hp = re.compile('#.*')
        pp=re.compile("'(/videos/expander.+?)'")
        while 1:
            #parse html tree
            js=common.getHTML(common.args.url)
    #        print "js", js
            js=js.split('");\n')
            pages=js[0]
            js=js[1]
            js=js.replace('\\u003c','<')
            js=js.replace('\\u003e','>')
            js=js.replace('\\"', '"')
            tree=BeautifulStoneSoup(js, convertEntities=BeautifulStoneSoup.HTML_ENTITIES)
            eps=tree.findAll('tr')[1:] # skip first header row
            for item in eps:
                cols=item.findAll('td')
                url=cols[1].contents[0]
                name=url.contents[0]
                url=url['href']
                url=hp.sub('',url)
                duration=cols[3].contents[0]
                airdate=cols[4].contents[0]
                #print name, url, duration, airdate
                match=p.findall(url)
                vid=match[0]
                info='http://www.hulu.com/videos/info/'+vid
                json=common.getHTML(info)
                match = dp.findall(json)
                plot=match[0]
                match = tp.findall(json)
                thumb=match[0]
                #print plot, thumb
                common.addDirectory(name, url, 'TV_Clips_play', thumb, thumb, common.args.fanart, plot, common.args.genre )

            pages=pages.replace('\\u003c','<')
            pages=pages.replace('\\u003e','>')
            pages=pages.replace('\\"', '"')
            tree=BeautifulStoneSoup(pages, convertEntities=BeautifulStoneSoup.HTML_ENTITIES)
            next=tree.findAll('li','btn')[1]#contains two arrows to navigate pages, chose the forward arrow
            if next.a: #contains a link for the next page
                url=pp.findall(next.a['onclick'])[0]
                url="http://hulu.com"+url
                common.args.url=url
            else:
                break
        xbmcplugin.endOfDirectory( handle=int( sys.argv[ 1 ] ))


    def addClipsList( self ):
        name = common.args.name.replace(xbmc.getLocalizedString(30091),'')
        tree=BeautifulStoneSoup(common.getHTML(common.args.url), convertEntities=BeautifulStoneSoup.HTML_ENTITIES)
        clips = tree.findAll('item')
        print clips
        p=re.compile('<p>(.+?)</p>.+?Added: ')
        for clip in clips:
            name = clip.title.contents[0].split('- ')[1:][0]
            url  = clip.link.contents[0].split('#')[0]
            try:
                thumb = clip.findAll('media:thumbnail')[0]['url']
            except:
                thumb = common.args.fanart
            try:
                plot =''.join(p.findall(str(clip.findAll('description')))).replace('<br />','\n').replace('Added: ','\n\nAdded: ')
            except:
                plot = ''
            common.addDirectory(name, url, 'TV_Clips_play', thumb, thumb, common.args.fanart, plot, common.args.genre )
        xbmcplugin.endOfDirectory( handle=int( sys.argv[ 1 ] ))
