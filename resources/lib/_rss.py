import xbmcplugin
import common
import sys
import re
from BeautifulSoup import BeautifulStoneSoup

class Main:

    def __init__( self ):
        if common.args.mode == 'RSS_Shows':
            self.activateShowPage()
        else:
            xbmcplugin.setContent(int(sys.argv[1]), 'tvshows')
            self.addListings()
            xbmcplugin.endOfDirectory( handle=int( sys.argv[ 1 ] ))


    def activateShowPage( self ):
        import _tv as tv
        common.args.mode = "TV_Seasons"
        tv.Main()


    def addListings ( self ):
        xmlsoup = BeautifulStoneSoup(common.getHTML( common.args.url ), convertEntities="html")
        items = xmlsoup.findAll('item')
        dp = re.compile('<p>(.+?)</p>')
        tp = re.compile('media:thumbnail.+?url="(.+?)"')
        for item in items:
            name = item.title.contents[0]
            url  = item.guid.contents[0]
            try:
                try:
                    plot = dp.findall(item.description.contents[0])[0]
                except:
                    plot = dp.findall(str(item.description))[0]
            except:
                plot = 'Unavailable'
            try:
                thumb = tp.findall(str(item))[0]
            except:
                thumb = ''

            name=common.cleanNames(name)
            plot=common.cleanNames(plot)

            try:
                fanart = 'http://assets.hulu.com/shows/key_art_'+name.split(':')[0].replace('-','_').replace(' ','_').replace('\'','').replace('"','').lower()+'.jpg'
            except:
                fanart = ''

            genre = common.args.name

            if common.args.name == 'Recently Added Shows' or common.args.name == 'My Subscriptions':
                common.addDirectory(name, url, 'RSS_Shows', thumb, thumb, fanart, plot, genre)
            else:
                common.addDirectory(name, url, 'RSS_play', thumb, thumb, fanart, plot, genre)
